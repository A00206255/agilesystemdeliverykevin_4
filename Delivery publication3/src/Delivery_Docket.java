
import java.awt.EventQueue;
/*import DATABASE.Database;
import Process.Docket;*/

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.border.BevelBorder;

public class Delivery_Docket {

	JFrame frame;
	private JTextField textField2;	
	private JTextField textField3;
	private JTextField textField5;
	static Database dbase=new Database();
	static Docket dock = null;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 */
	public Delivery_Docket(Database db, Docket d) 
	{
		dbase = db;
		dock = d;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 558, 424);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Delivery Area:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(77, 122, 180, 33);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Delivery Person ID:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel_1.setBounds(77, 175, 161, 33);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblSubscriptionId = new JLabel("Customer ID:");
		lblSubscriptionId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblSubscriptionId.setBounds(77, 241, 180, 31);
		frame.getContentPane().add(lblSubscriptionId);
		
		textField2 = new JTextField();
		textField2.setBorder(new LineBorder(new Color(171, 173, 179), 2, true));
		textField2.setColumns(10);
		textField2.setBounds(303, 124, 161, 33);
		frame.getContentPane().add(textField2);
		
		textField3 = new JTextField();
		textField3.setBorder(new LineBorder(new Color(171, 173, 179), 2, true));
		textField3.setColumns(10);
		textField3.setBounds(303, 176, 161, 33);
		frame.getContentPane().add(textField3);
		
		textField5 = new JTextField();
		textField5.setBorder(new LineBorder(new Color(171, 173, 179), 2, true));
		textField5.setColumns(10);
		textField5.setBounds(303, 239, 161, 33);
		frame.getContentPane().add(textField5);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				 if ( textField2.getText().matches(".*[^a-zA-Z0-9]+.*"))
				{  
					JOptionPane.showMessageDialog(null, "Invaild. Your delivery area should consist of  letters and digits only.");
					System.out.println(textField2.getText());
				}

				else {
				
				dock = new Docket(textField2.getText(), Integer.parseInt(textField3.getText()), Integer.parseInt(textField5.getText()), dbase);
				
				boolean res = dock.insertDocket();
				
				if(res == true)
				{
					JOptionPane.showMessageDialog(null, "Insert Successful");
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Insert Failed");
				}
				
			}
			}
		});
		btnSubmit.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnSubmit.setBounds(66, 309, 178, 40);
		frame.getContentPane().add(btnSubmit);
		
		JLabel lblDailyDeliveryDockets = new JLabel("Daily Delivery Dockets");
		lblDailyDeliveryDockets.setFont(new Font("Yu Gothic Light", Font.BOLD, 18));
		lblDailyDeliveryDockets.setBounds(185, 57, 212, 26);
		frame.getContentPane().add(lblDailyDeliveryDockets);
		
		JButton button = new JButton("Cancel");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				//Code to open the main menu			
				MainMenu main = new MainMenu();
				main.frame.setVisible(true);
				frame.setVisible(false); 
				frame.dispose(); 
			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 18));
		button.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		button.setBounds(286, 309, 178, 40);
		frame.getContentPane().add(button);
		
		
	}
}
